/* apps/ca.c */
/* Copyright (C) 1995-1998 Eric Young (eay@cryptsoft.com)
 * All rights reserved.
 *
 * This package is an SSL implementation written
 * by Eric Young (eay@cryptsoft.com).
 * The implementation was written so as to conform with Netscapes SSL.
 *
 * This library is free for commercial and non-commercial use as long as
 * the following conditions are aheared to.  The following conditions
 * apply to all code found in this distribution, be it the RC4, RSA,
 * lhash, DES, etc., code; not just the SSL code.  The SSL documentation
 * included with this distribution is covered by the same copyright terms
 * except that the holder is Tim Hudson (tjh@cryptsoft.com).
 *
 * Copyright remains Eric Young's, and as such any Copyright notices in
 * the code are not to be removed.
 * If this package is used in a product, Eric Young should be given attribution
 * as the author of the parts of the library used.
 * This can be in the form of a textual message at program startup or
 * in documentation (online or textual) provided with the package.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    "This product includes cryptographic software written by
 *     Eric Young (eay@cryptsoft.com)"
 *    The word 'cryptographic' can be left out if the rouines from the library
 *    being used are not cryptographic related :-).
 * 4. If you include any Windows specific code (or a derivative thereof) from
 *    the apps directory (application code) you must include an acknowledgement:
 *    "This product includes software written by Tim Hudson (tjh@cryptsoft.com)"
 *
 * THIS SOFTWARE IS PROVIDED BY ERIC YOUNG ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * The licence and distribution terms for any publically available version or
 * derivative of this code cannot be changed.  i.e. this code cannot simply be
 * copied and put under another distribution licence
 * [including the GNU Public Licence.]
 */

/* The PPKI stuff has been donated by Jeff Barber <jeffb@issl.atl.hp.com> */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <openssl/conf.h>
#include <openssl/bio.h>
#include <openssl/err.h>
#include <openssl/bn.h>
#include <openssl/txt_db.h>
#include <openssl/evp.h>
#include <openssl/x509.h>
#include <openssl/x509v3.h>
#include <openssl/objects.h>
#include <openssl/ocsp.h>
#include <openssl/pem.h>

#ifndef W_OK
# ifdef OPENSSL_SYS_VMS
#  if defined(__DECC)
#   include <unistd.h>
#  else
#   include <unixlib.h>
#  endif
# elif !defined(OPENSSL_SYS_VXWORKS) && !defined(OPENSSL_SYS_WINDOWS) && !defined(OPENSSL_SYS_NETWARE)
#  include <sys/file.h>
# endif
#endif

#include "apps.h"

#ifndef W_OK
# define F_OK 0
# define X_OK 1
# define W_OK 2
# define R_OK 4
#endif

#undef PROG
#define PROG ca_main

#define BASE_SECTION    "ca"
#define CONFIG_FILE "openssl.cnf"

#define ENV_DEFAULT_CA          "default_ca"

#define STRING_MASK     "string_mask"
#define UTF8_IN                 "utf8"

#define ENV_DIR                 "dir"
#define ENV_CERTS               "certs"
#define ENV_CRL_DIR             "crl_dir"
#define ENV_CA_DB               "CA_DB"
#define ENV_NEW_CERTS_DIR       "new_certs_dir"
#define ENV_CERTIFICATE         "certificate"
#define ENV_SERIAL              "serial"
#define ENV_CRLNUMBER           "crlnumber"
#define ENV_CRL                 "crl"
#define ENV_PRIVATE_KEY         "private_key"
#define ENV_RANDFILE            "RANDFILE"
#define ENV_DEFAULT_CRL_DAYS    "default_crl_days"
#define ENV_DEFAULT_CRL_HOURS   "default_crl_hours"
#define ENV_DEFAULT_MD          "default_md"
#define ENV_EXTENSIONS          "x509_extensions"
#define ENV_CRLEXT              "crl_extensions"
#define ENV_NAMEOPT             "name_opt"
#define ENV_CERTOPT             "cert_opt"
#define ENV_EXTCOPY             "copy_extensions"
#define ENV_UNIQUE_SUBJECT      "unique_subject"

#define ENV_DATABASE            "database"

/* Additional revocation information types */

#define REV_NONE                0 /* No addditional information */
#define REV_CRL_REASON          1 /* Value is CRL reason code */
#define REV_HOLD                2 /* Value is hold instruction */
#define REV_KEY_COMPROMISE      3 /* Value is cert key compromise time */
#define REV_CA_COMPROMISE       4 /* Value is CA key compromise time */

CONF *config = NULL;
BIO *bio_err = NULL;

static const char *ca_usage[] = {
    "usage: ca args\n",
    "\n",
    " -verbose        - Talk alot while doing things\n",
    " -config file    - A config file\n",
    " -name arg       - The particular CA definition to use\n",
    " -crldays days   - Days is when the next CRL is due\n",
    " -crlhours hours - Hours is when the next CRL is due\n",
    " -md arg         - md to use, one of md2, md5, sha or sha1\n",
    " -keyfile arg    - private key file\n",
    " -keyform arg    - private key file format (PEM or ENGINE)\n",
    " -key arg        - key to decode the private key if it is encrypted\n",
    " -cert file      - The CA certificate\n",
    " -out file       - Where to put the output file(s)\n",
    " -outdir dir     - Where to put output certificates\n",
    " -utf8           - input characters are UTF8 (default ASCII)\n",
    " -extensions ..  - Extension section (override value in config file)\n",
    " -extfile file   - Configuration file with X509v3 extentions to add\n",
    " -crlexts ..     - CRL extension section (override value in config file)\n",
    NULL
};

#ifdef EFENCE
extern int EF_PROTECT_FREE;
extern int EF_PROTECT_BELOW;
extern int EF_ALIGNMENT;
#endif

static void lookup_fail(const char *name, const char *tag);
static int check_time_format(const char *str);
static int do_sign_init(BIO *err, EVP_MD_CTX *ctx, EVP_PKEY *pkey,
                        const EVP_MD *md, STACK_OF(OPENSSL_STRING) *sigopts);
int make_revoked(X509_REVOKED *rev, const char *str);
int old_entry_print(BIO *bp, ASN1_OBJECT *obj, ASN1_STRING *str);
static CONF *conf = NULL;
static CONF *extconf = NULL;
static char *section = NULL;

int main(int argc, char **argv)
{
    char *key = NULL, *passargin = NULL;
    int free_key = 0;
    int badops = 0;
    int ret = 1;
    int verbose = 0;
    long crldays = 0;
    long crlhours = 0;
    long crlsec = 0;
    long errorline = -1;
    char *configfile = NULL;
    char *md = NULL;
    char *keyfile = NULL;
    char *certfile = NULL;
    int keyform = FORMAT_PEM;
    EVP_PKEY *pkey = NULL;
    char *outfile = NULL;
    char *crlnumberfile = NULL;
    char *extensions = NULL;
    char *extfile = NULL;
    unsigned long chtype = MBSTRING_ASC;
    char *crl_ext = NULL;
    BIGNUM *serial = NULL;
    BIGNUM *crlnumber = NULL;
    unsigned long nameopt = 0, certopt = 0;
    int ext_copy = EXT_COPY_NONE;
    X509 *x509 = NULL;
    BIO *in = NULL, *out = NULL, *Sout = NULL, *Cout = NULL;
    char *dbfile = NULL;
    CA_DB *db = NULL;
    X509_CRL *crl = NULL;
    X509_REVOKED *r = NULL;
    ASN1_TIME *tmptm;
    ASN1_INTEGER *tmpser;
    char *f;
    const char *p;
    char *const *pp;
    int i, j;
    const EVP_MD *dgst = NULL;
    STACK_OF(X509) *cert_sk = NULL;
    STACK_OF(OPENSSL_STRING) *sigopts = NULL;
#undef BSIZE
#define BSIZE 256
    char *randfile = NULL;
    char *tofree = NULL;
    DB_ATTR db_attr;

#ifdef EFENCE
    EF_PROTECT_FREE = 1;
    EF_PROTECT_BELOW = 1;
    EF_ALIGNMENT = 0;
#endif

    apps_startup();

    conf = NULL;
    key = NULL;
    section = NULL;

    if (bio_err == NULL)
        if ((bio_err = BIO_new(BIO_s_file())) != NULL)
            BIO_set_fp(bio_err, stderr, BIO_NOCLOSE | BIO_FP_TEXT);

    argc--;
    argv++;
    while (argc >= 1) {
        if (strcmp(*argv, "-verbose") == 0)
            verbose = 1;
        else if (strcmp(*argv, "-config") == 0) {
            if (--argc < 1)
                goto bad;
            configfile = *(++argv);
        } else if (strcmp(*argv, "-name") == 0) {
            if (--argc < 1)
                goto bad;
            section = *(++argv);
        } else if (strcmp(*argv, "-utf8") == 0) {
            chtype = MBSTRING_UTF8;
        } else if (strcmp(*argv, "-md") == 0) {
            if (--argc < 1)
                goto bad;
            md = *(++argv);
        } else if (strcmp(*argv, "-keyfile") == 0) {
            if (--argc < 1)
                goto bad;
            keyfile = *(++argv);
        } else if (strcmp(*argv, "-keyform") == 0) {
            if (--argc < 1)
                goto bad;
            keyform = str2fmt(*(++argv));
        } else if (strcmp(*argv, "-passin") == 0) {
            if (--argc < 1)
                goto bad;
            passargin = *(++argv);
        } else if (strcmp(*argv, "-key") == 0) {
            if (--argc < 1)
                goto bad;
            key = *(++argv);
        } else if (strcmp(*argv, "-cert") == 0) {
            if (--argc < 1)
                goto bad;
            certfile = *(++argv);
        } else if (strcmp(*argv, "-out") == 0) {
            if (--argc < 1)
                goto bad;
            outfile = *(++argv);
        } else if (strcmp(*argv, "-sigopt") == 0) {
            if (--argc < 1)
                goto bad;
            if (!sigopts)
                sigopts = sk_OPENSSL_STRING_new_null();
            if (!sigopts || !sk_OPENSSL_STRING_push(sigopts, *(++argv)))
                goto bad;
        } else if (strcmp(*argv, "-crldays") == 0) {
            if (--argc < 1)
                goto bad;
            crldays = atol(*(++argv));
        } else if (strcmp(*argv, "-crlhours") == 0) {
            if (--argc < 1)
                goto bad;
            crlhours = atol(*(++argv));
        } else if (strcmp(*argv, "-crlsec") == 0) {
            if (--argc < 1)
                goto bad;
            crlsec = atol(*(++argv));
        } else if (strcmp(*argv, "-extensions") == 0) {
            if (--argc < 1)
                goto bad;
            extensions = *(++argv);
        } else if (strcmp(*argv, "-extfile") == 0) {
            if (--argc < 1)
                goto bad;
            extfile = *(++argv);
        } else if (strcmp(*argv, "-crlexts") == 0) {
            if (--argc < 1)
                goto bad;
            crl_ext = *(++argv);
        } else {
 bad:
            BIO_printf(bio_err, "unknown option %s\n", *argv);
            badops = 1;
            break;
        }
        argc--;
        argv++;
    }

    if (badops) {
        const char **pp2;

        for (pp2 = ca_usage; (*pp2 != NULL); pp2++)
            BIO_printf(bio_err, "%s", *pp2);
        goto err;
    }

    ERR_load_crypto_strings();

        /*****************************************************************/
    tofree = NULL;
    if (configfile == NULL)
        configfile = getenv("OPENSSL_CONF");
    if (configfile == NULL)
        configfile = getenv("SSLEAY_CONF");
    if (configfile == NULL) {
        const char *s = X509_get_default_cert_area();
        size_t len;

#ifdef OPENSSL_SYS_VMS
        len = strlen(s) + sizeof(CONFIG_FILE);
        tofree = OPENSSL_malloc(len);
        if(!tofree) {
            BIO_printf(bio_err, "Out of memory\n");
            goto err;
        }
        strcpy(tofree, s);
#else
        len = strlen(s) + sizeof(CONFIG_FILE) + 1;
        tofree = OPENSSL_malloc(len);
        if(!tofree) {
            BIO_printf(bio_err, "Out of memory\n");
            goto err;
        }
        BUF_strlcpy(tofree, s, len);
        BUF_strlcat(tofree, "/", len);
#endif
        BUF_strlcat(tofree, CONFIG_FILE, len);
        configfile = tofree;
    }

    BIO_printf(bio_err, "Using configuration from %s\n", configfile);
    conf = NCONF_new(NULL);
    if (NCONF_load(conf, configfile, &errorline) <= 0) {
        if (errorline <= 0)
            BIO_printf(bio_err, "error loading the config file '%s'\n",
                       configfile);
        else
            BIO_printf(bio_err, "error on line %ld of config file '%s'\n",
                       errorline, configfile);
        goto err;
    }
    if (tofree) {
        OPENSSL_free(tofree);
        tofree = NULL;
    }

    if (!load_config(bio_err, conf))
        goto err;

    /* Lets get the config section we are using */
    if (section == NULL) {
        section = NCONF_get_string(conf, BASE_SECTION, ENV_DEFAULT_CA);
        if (section == NULL) {
            lookup_fail(BASE_SECTION, ENV_DEFAULT_CA);
            goto err;
        }
    }

    if (conf != NULL) {
        p = NCONF_get_string(conf, NULL, "oid_file");
        if (p == NULL)
            ERR_clear_error();
        if (p != NULL) {
            BIO *oid_bio;

            oid_bio = BIO_new_file(p, "r");
            if (oid_bio == NULL) {
                /*-
                BIO_printf(bio_err,"problems opening %s for extra oid's\n",p);
                ERR_print_errors(bio_err);
                */
                ERR_clear_error();
            } else {
                OBJ_create_objects(oid_bio);
                BIO_free(oid_bio);
            }
        }
        if (!add_oid_section(bio_err, conf)) {
            ERR_print_errors(bio_err);
            goto err;
        }
    }

    randfile = NCONF_get_string(conf, BASE_SECTION, "RANDFILE");
    if (randfile == NULL)
        ERR_clear_error();
    app_RAND_load_file(randfile, bio_err, 0);

    f = NCONF_get_string(conf, section, STRING_MASK);
    if (!f)
        ERR_clear_error();

    if (f && !ASN1_STRING_set_default_mask_asc(f)) {
        BIO_printf(bio_err, "Invalid global string mask setting %s\n", f);
        goto err;
    }

    if (chtype != MBSTRING_UTF8) {
        f = NCONF_get_string(conf, section, UTF8_IN);
        if (!f)
            ERR_clear_error();
        else if (!strcmp(f, "yes"))
            chtype = MBSTRING_UTF8;
    }

    db_attr.unique_subject = 1;
    p = NCONF_get_string(conf, section, ENV_UNIQUE_SUBJECT);
    if (p) {
#ifdef RL_DEBUG
        BIO_printf(bio_err, "DEBUG: unique_subject = \"%s\"\n", p);
#endif
        db_attr.unique_subject = parse_yesno(p, 1);
    } else
        ERR_clear_error();
#ifdef RL_DEBUG
    if (!p)
        BIO_printf(bio_err, "DEBUG: unique_subject undefined\n");
#endif
#ifdef RL_DEBUG
    BIO_printf(bio_err, "DEBUG: configured unique_subject is %d\n",
               db_attr.unique_subject);
#endif

    in = BIO_new(BIO_s_file());
    out = BIO_new(BIO_s_file());
    Sout = BIO_new(BIO_s_file());
    Cout = BIO_new(BIO_s_file());
    if ((in == NULL) || (out == NULL) || (Sout == NULL) || (Cout == NULL)) {
        ERR_print_errors(bio_err);
        goto err;
    }

    /*****************************************************************/
    /* we definitely need a private key, so let's get it */

    if ((keyfile == NULL) && ((keyfile = NCONF_get_string(conf,
                                                          section,
                                                          ENV_PRIVATE_KEY)) ==
                              NULL)) {
        lookup_fail(section, ENV_PRIVATE_KEY);
        goto err;
    }
    if (!key) {
        free_key = 1;
        if (!app_passwd(bio_err, passargin, NULL, &key, NULL)) {
            BIO_printf(bio_err, "Error getting password\n");
            goto err;
        }
    }
    pkey = load_key(bio_err, keyfile, keyform, 0, key, NULL, "CA private key");
    if (key)
        OPENSSL_cleanse(key, strlen(key));
    if (pkey == NULL) {
        /* load_key() has already printed an appropriate message */
        goto err;
    }

        /*****************************************************************/
    /* we need a certificate */
    if ((certfile == NULL)
        && ((certfile = NCONF_get_string(conf,
                                         section,
                                         ENV_CERTIFICATE)) == NULL)) {
        lookup_fail(section, ENV_CERTIFICATE);
        goto err;
    }
    x509 = load_cert(bio_err, certfile, FORMAT_PEM, NULL, NULL,
                     "CA certificate");
    if (x509 == NULL)
        goto err;

    if (!X509_check_private_key(x509, pkey)) {
        BIO_printf(bio_err,
                   "CA certificate and CA private key do not match\n");
        goto err;
    }

    f = NCONF_get_string(conf, section, ENV_NAMEOPT);

    if (f) {
        if (!set_name_ex(&nameopt, f)) {
            BIO_printf(bio_err, "Invalid name options: \"%s\"\n", f);
            goto err;
        }
    } else
        ERR_clear_error();

    f = NCONF_get_string(conf, section, ENV_CERTOPT);

    if (f) {
        if (!set_cert_ex(&certopt, f)) {
            BIO_printf(bio_err, "Invalid certificate options: \"%s\"\n", f);
            goto err;
        }
    } else
        ERR_clear_error();

    f = NCONF_get_string(conf, section, ENV_EXTCOPY);

    if (f) {
        if (!set_ext_copy(&ext_copy, f)) {
            BIO_printf(bio_err, "Invalid extension copy option: \"%s\"\n", f);
            goto err;
        }
    } else
        ERR_clear_error();

    /*****************************************************************/
    /* we need to load the database file */
    if ((dbfile = NCONF_get_string(conf, section, ENV_DATABASE)) == NULL) {
        lookup_fail(section, ENV_DATABASE);
        goto err;
    }
    db = load_index(dbfile, &db_attr);
    if (db == NULL)
        goto err;

    /* Lets check some fields */
    for (i = 0; i < sk_OPENSSL_PSTRING_num(db->db->data); i++) {
        pp = sk_OPENSSL_PSTRING_value(db->db->data, i);
        if ((pp[DB_type][0] != DB_TYPE_REV) && (pp[DB_rev_date][0] != '\0')) {
            BIO_printf(bio_err,
                       "entry %d: not revoked yet, but has a revocation date\n",
                       i + 1);
            goto err;
        }
        if ((pp[DB_type][0] == DB_TYPE_REV) &&
            !make_revoked(NULL, pp[DB_rev_date])) {
            BIO_printf(bio_err, " in entry %d\n", i + 1);
            goto err;
        }
        if (!check_time_format((char *)pp[DB_exp_date])) {
            BIO_printf(bio_err, "entry %d: invalid expiry date\n", i + 1);
            goto err;
        }
        p = pp[DB_serial];
        j = strlen(p);
        if (*p == '-') {
            p++;
            j--;
        }
        if ((j & 1) || (j < 2)) {
            BIO_printf(bio_err, "entry %d: bad serial number length (%d)\n",
                       i + 1, j);
            goto err;
        }
        while (*p) {
            if (!(((*p >= '0') && (*p <= '9')) ||
                  ((*p >= 'A') && (*p <= 'F')) ||
                  ((*p >= 'a') && (*p <= 'f')))) {
                BIO_printf(bio_err,
                           "entry %d: bad serial number characters, char pos %ld, char is '%c'\n",
                           i + 1, (long)(p - pp[DB_serial]), *p);
                goto err;
            }
            p++;
        }
    }
    if (verbose) {
        BIO_set_fp(out, stdout, BIO_NOCLOSE | BIO_FP_TEXT); /* cannot fail */
#ifdef OPENSSL_SYS_VMS
        {
            BIO *tmpbio = BIO_new(BIO_f_linebuffer());
            out = BIO_push(tmpbio, out);
        }
#endif
        TXT_DB_write(out, db->db);
        BIO_printf(bio_err, "%d entries loaded from the database\n",
                   sk_OPENSSL_PSTRING_num(db->db->data));
        BIO_printf(bio_err, "generating index\n");
    }

    if (!index_index(db))
        goto err;


    /*****************************************************************/
    /* Read extentions config file                                   */
    if (extfile) {
        extconf = NCONF_new(NULL);
        if (NCONF_load(extconf, extfile, &errorline) <= 0) {
            if (errorline <= 0)
                BIO_printf(bio_err, "ERROR: loading the config file '%s'\n",
                           extfile);
            else
                BIO_printf(bio_err,
                           "ERROR: on line %ld of config file '%s'\n",
                           errorline, extfile);
            ret = 1;
            goto err;
        }

        if (verbose)
            BIO_printf(bio_err, "Successfully loaded extensions file %s\n",
                       extfile);

        /* We can have sections in the ext file */
        if (!extensions
            && !(extensions =
                 NCONF_get_string(extconf, "default", "extensions")))
            extensions = "default";
    }

    /*****************************************************************/
    if (outfile != NULL) {
        if (BIO_write_filename(Sout, outfile) <= 0) {
            perror(outfile);
            goto err;
        }
    } else {
        BIO_set_fp(Sout, stdout, BIO_NOCLOSE | BIO_FP_TEXT);
#ifdef OPENSSL_SYS_VMS
        {
            BIO *tmpbio = BIO_new(BIO_f_linebuffer());
            Sout = BIO_push(tmpbio, Sout);
        }
#endif
    }

    if ((md == NULL) && ((md = NCONF_get_string(conf,
                                                section,
                                                ENV_DEFAULT_MD)) == NULL)) {
        lookup_fail(section, ENV_DEFAULT_MD);
        goto err;
    }

    if (!strcmp(md, "default")) {
        int def_nid;
        if (EVP_PKEY_get_default_digest_nid(pkey, &def_nid) <= 0) {
            BIO_puts(bio_err, "no default digest\n");
            goto err;
        }
        md = (char *)OBJ_nid2sn(def_nid);
    }

    if ((dgst = EVP_get_digestbyname(md)) == NULL) {
        BIO_printf(bio_err, "%s is an unsupported message digest type\n", md);
        goto err;
    }

    /*****************************************************************/
    if (!crl_ext) {
        crl_ext = NCONF_get_string(conf, section, ENV_CRLEXT);
        if (!crl_ext)
        ERR_clear_error();
    }
    if (crl_ext) {
        /* Check syntax of file */
        X509V3_CTX ctx;
        X509V3_set_ctx_test(&ctx);
        X509V3_set_nconf(&ctx, conf);
        if (!X509V3_EXT_add_nconf(conf, &ctx, crl_ext, NULL)) {
        BIO_printf(bio_err,
               "Error Loading CRL extension section %s\n",
               crl_ext);
        ret = 1;
        goto err;
        }
    }

    if ((crlnumberfile = NCONF_get_string(conf, section, ENV_CRLNUMBER))
        != NULL)
        if ((crlnumber = load_serial(crlnumberfile, 0, NULL)) == NULL) {
        BIO_printf(bio_err, "error while loading CRL number\n");
        goto err;
        }

    if (!crldays && !crlhours && !crlsec) {
        if (!NCONF_get_number(conf, section,
                  ENV_DEFAULT_CRL_DAYS, &crldays))
        crldays = 0;
        if (!NCONF_get_number(conf, section,
                  ENV_DEFAULT_CRL_HOURS, &crlhours))
        crlhours = 0;
        ERR_clear_error();
    }
    if ((crldays == 0) && (crlhours == 0) && (crlsec == 0)) {
        BIO_printf(bio_err,
               "cannot lookup how long until the next CRL is issued\n");
        goto err;
    }

    if (verbose)
        BIO_printf(bio_err, "making CRL\n");
    if ((crl = X509_CRL_new()) == NULL)
        goto err;
    if (!X509_CRL_set_issuer_name(crl, X509_get_subject_name(x509)))
        goto err;

    tmptm = ASN1_TIME_new();
    if (!tmptm)
        goto err;
    X509_gmtime_adj(tmptm, 0);
    X509_CRL_set_lastUpdate(crl, tmptm);
    if (!X509_time_adj_ex(tmptm, crldays, crlhours * 60 * 60 + crlsec,
                  NULL)) {
        BIO_puts(bio_err, "error setting CRL nextUpdate\n");
        goto err;
    }
    X509_CRL_set_nextUpdate(crl, tmptm);

    ASN1_TIME_free(tmptm);

    for (i = 0; i < sk_OPENSSL_PSTRING_num(db->db->data); i++) {
        pp = sk_OPENSSL_PSTRING_value(db->db->data, i);
        if (pp[DB_type][0] == DB_TYPE_REV) {
        if ((r = X509_REVOKED_new()) == NULL)
            goto err;
        j = make_revoked(r, pp[DB_rev_date]);
        if (!j)
            goto err;
        if (!BN_hex2bn(&serial, pp[DB_serial]))
            goto err;
        tmpser = BN_to_ASN1_INTEGER(serial, NULL);
        BN_free(serial);
        serial = NULL;
        if (!tmpser)
            goto err;
        X509_REVOKED_set_serialNumber(r, tmpser);
        ASN1_INTEGER_free(tmpser);
        X509_CRL_add0_revoked(crl, r);
        }
    }

    /*
     * sort the data so it will be written in serial number order
     */
    X509_CRL_sort(crl);

    /* we now have a CRL */
    if (verbose)
        BIO_printf(bio_err, "signing CRL\n");

    /* Add any extensions asked for */

    if (crl_ext || crlnumberfile != NULL) {
        X509V3_CTX crlctx;
        X509V3_set_ctx(&crlctx, x509, NULL, NULL, crl, 0);
        X509V3_set_nconf(&crlctx, conf);

        if (crl_ext)
        if (!X509V3_EXT_CRL_add_nconf(conf, &crlctx, crl_ext, crl))
            goto err;
        if (crlnumberfile != NULL) {
        tmpser = BN_to_ASN1_INTEGER(crlnumber, NULL);
        if (!tmpser)
            goto err;
        X509_CRL_add1_ext_i2d(crl, NID_crl_number, tmpser, 0, 0);
        ASN1_INTEGER_free(tmpser);
        if (!BN_add_word(crlnumber, 1))
            goto err;
        }
    }
    
    /* we support only crl_v2 */
    if (!X509_CRL_set_version(crl, 1))
    goto err;       /* version 2 CRL */
   

    /* we have a CRL number that need updating */
    if (crlnumberfile != NULL)
        if (!save_serial(crlnumberfile, "new", crlnumber, NULL))
        goto err;

    if (crlnumber) {
        BN_free(crlnumber);
        crlnumber = NULL;
    }

    if (!do_X509_CRL_sign(bio_err, crl, pkey, dgst, sigopts))
        goto err;

    PEM_write_bio_X509_CRL(Sout, crl);

    if (crlnumberfile != NULL) /* Rename the crlnumber file */
        if (!rotate_serial(crlnumberfile, "new", "old"))
        goto err;


    /*****************************************************************/
    ret = 0;
 err:
    if (tofree)
        OPENSSL_free(tofree);
    BIO_free_all(Cout);
    BIO_free_all(Sout);
    BIO_free_all(out);
    BIO_free_all(in);

    if (cert_sk)
        sk_X509_pop_free(cert_sk, X509_free);

    if (ret)
        ERR_print_errors(bio_err);
    app_RAND_write_file(randfile, bio_err);
    if (free_key && key)
        OPENSSL_free(key);
    BN_free(serial);
    BN_free(crlnumber);
    free_index(db);
    if (sigopts)
        sk_OPENSSL_STRING_free(sigopts);
    EVP_PKEY_free(pkey);
    if (x509)
        X509_free(x509);
    X509_CRL_free(crl);
    NCONF_free(conf);
    NCONF_free(extconf);
    OBJ_cleanup();
    apps_shutdown();
    OPENSSL_EXIT(ret);
}

static void lookup_fail(const char *name, const char *tag)
{
    BIO_printf(bio_err, "variable lookup failed for %s::%s\n", name, tag);
}

static int check_time_format(const char *str)
{
    return ASN1_TIME_set_string(NULL, str);
}

static const char *crl_reasons[] = {
    /* CRL reason strings */
    "unspecified",
    "keyCompromise",
    "CACompromise",
    "affiliationChanged",
    "superseded",
    "cessationOfOperation",
    "certificateHold",
    "removeFromCRL",
    /* Additional pseudo reasons */
    "holdInstruction",
    "keyTime",
    "CAkeyTime"
};

#define NUM_REASONS (sizeof(crl_reasons) / sizeof(char *))

/*-
 * Convert revocation field to X509_REVOKED entry
 * return code:
 * 0 error
 * 1 OK
 * 2 OK and some extensions added (i.e. V2 CRL)
 */

int make_revoked(X509_REVOKED *rev, const char *str)
{
    char *tmp = NULL;
    int reason_code = -1;
    int i, ret = 0;
    ASN1_OBJECT *hold = NULL;
    ASN1_GENERALIZEDTIME *comp_time = NULL;
    ASN1_ENUMERATED *rtmp = NULL;

    ASN1_TIME *revDate = NULL;

    i = unpack_revinfo(&revDate, &reason_code, &hold, &comp_time, str);

    if (i == 0)
        goto err;

    if (rev && !X509_REVOKED_set_revocationDate(rev, revDate))
        goto err;

    if (rev && (reason_code != OCSP_REVOKED_STATUS_NOSTATUS)) {
        rtmp = ASN1_ENUMERATED_new();
        if (!rtmp || !ASN1_ENUMERATED_set(rtmp, reason_code))
            goto err;
        if (!X509_REVOKED_add1_ext_i2d(rev, NID_crl_reason, rtmp, 0, 0))
            goto err;
    }

    if (rev && comp_time) {
        if (!X509_REVOKED_add1_ext_i2d
            (rev, NID_invalidity_date, comp_time, 0, 0))
            goto err;
    }
    if (rev && hold) {
        if (!X509_REVOKED_add1_ext_i2d
            (rev, NID_hold_instruction_code, hold, 0, 0))
            goto err;
    }

    if (reason_code != OCSP_REVOKED_STATUS_NOSTATUS)
        ret = 2;
    else
        ret = 1;

 err:

    if (tmp)
        OPENSSL_free(tmp);
    ASN1_OBJECT_free(hold);
    ASN1_GENERALIZEDTIME_free(comp_time);
    ASN1_ENUMERATED_free(rtmp);
    ASN1_TIME_free(revDate);

    return ret;
}

int old_entry_print(BIO *bp, ASN1_OBJECT *obj, ASN1_STRING *str)
{
    char buf[25], *pbuf, *p;
    int j;
    j = i2a_ASN1_OBJECT(bp, obj);
    pbuf = buf;
    for (j = 22 - j; j > 0; j--)
        *(pbuf++) = ' ';
    *(pbuf++) = ':';
    *(pbuf++) = '\0';
    BIO_puts(bp, buf);

    if (str->type == V_ASN1_PRINTABLESTRING)
        BIO_printf(bp, "PRINTABLE:'");
    else if (str->type == V_ASN1_T61STRING)
        BIO_printf(bp, "T61STRING:'");
    else if (str->type == V_ASN1_IA5STRING)
        BIO_printf(bp, "IA5STRING:'");
    else if (str->type == V_ASN1_UNIVERSALSTRING)
        BIO_printf(bp, "UNIVERSALSTRING:'");
    else
        BIO_printf(bp, "ASN.1 %2d:'", str->type);

    p = (char *)str->data;
    for (j = str->length; j > 0; j--) {
        if ((*p >= ' ') && (*p <= '~'))
            BIO_printf(bp, "%c", *p);
        else if (*p & 0x80)
            BIO_printf(bp, "\\0x%02X", *p);
        else if ((unsigned char)*p == 0xf7)
            BIO_printf(bp, "^?");
        else
            BIO_printf(bp, "^%c", *p + '@');
        p++;
    }
    BIO_printf(bp, "'\n");
    return 1;
}

int unpack_revinfo(ASN1_TIME **prevtm, int *preason, ASN1_OBJECT **phold,
                   ASN1_GENERALIZEDTIME **pinvtm, const char *str)
{
    char *tmp = NULL;
    char *rtime_str, *reason_str = NULL, *arg_str = NULL, *p;
    int reason_code = -1;
    int ret = 0;
    unsigned int i;
    ASN1_OBJECT *hold = NULL;
    ASN1_GENERALIZEDTIME *comp_time = NULL;
    tmp = BUF_strdup(str);

    if(!tmp) {
        BIO_printf(bio_err, "memory allocation failure\n");
        goto err;
    }

    p = strchr(tmp, ',');

    rtime_str = tmp;

    if (p) {
        *p = '\0';
        p++;
        reason_str = p;
        p = strchr(p, ',');
        if (p) {
            *p = '\0';
            arg_str = p + 1;
        }
    }

    if (prevtm) {
        *prevtm = ASN1_UTCTIME_new();
        if(!*prevtm) {
            BIO_printf(bio_err, "memory allocation failure\n");
            goto err;
        }
        if (!ASN1_UTCTIME_set_string(*prevtm, rtime_str)) {
            BIO_printf(bio_err, "invalid revocation date %s\n", rtime_str);
            goto err;
        }
    }
    if (reason_str) {
        for (i = 0; i < NUM_REASONS; i++) {
            if (!strcasecmp(reason_str, crl_reasons[i])) {
                reason_code = i;
                break;
            }
        }
        if (reason_code == OCSP_REVOKED_STATUS_NOSTATUS) {
            BIO_printf(bio_err, "invalid reason code %s\n", reason_str);
            goto err;
        }

        if (reason_code == 7)
            reason_code = OCSP_REVOKED_STATUS_REMOVEFROMCRL;
        else if (reason_code == 8) { /* Hold instruction */
            if (!arg_str) {
                BIO_printf(bio_err, "missing hold instruction\n");
                goto err;
            }
            reason_code = OCSP_REVOKED_STATUS_CERTIFICATEHOLD;
            hold = OBJ_txt2obj(arg_str, 0);

            if (!hold) {
                BIO_printf(bio_err, "invalid object identifier %s\n",
                           arg_str);
                goto err;
            }
            if (phold)
                *phold = hold;
        } else if ((reason_code == 9) || (reason_code == 10)) {
            if (!arg_str) {
                BIO_printf(bio_err, "missing compromised time\n");
                goto err;
            }
            comp_time = ASN1_GENERALIZEDTIME_new();
            if(!comp_time) {
                BIO_printf(bio_err, "memory allocation failure\n");
                goto err;
            }
            if (!ASN1_GENERALIZEDTIME_set_string(comp_time, arg_str)) {
                BIO_printf(bio_err, "invalid compromised time %s\n", arg_str);
                goto err;
            }
            if (reason_code == 9)
                reason_code = OCSP_REVOKED_STATUS_KEYCOMPROMISE;
            else
                reason_code = OCSP_REVOKED_STATUS_CACOMPROMISE;
        }
    }

    if (preason)
        *preason = reason_code;
    if (pinvtm)
        *pinvtm = comp_time;
    else
        ASN1_GENERALIZEDTIME_free(comp_time);

    ret = 1;

 err:

    if (tmp)
        OPENSSL_free(tmp);
    if (!phold)
        ASN1_OBJECT_free(hold);
    if (!pinvtm)
        ASN1_GENERALIZEDTIME_free(comp_time);

    return ret;
}

static int do_sign_init(BIO *err, EVP_MD_CTX *ctx, EVP_PKEY *pkey,
                        const EVP_MD *md, STACK_OF(OPENSSL_STRING) *sigopts)
{
    EVP_PKEY_CTX *pkctx = NULL;
    int i;
    EVP_MD_CTX_init(ctx);
    if (!EVP_DigestSignInit(ctx, &pkctx, md, NULL, pkey))
        return 0;
    for (i = 0; i < sk_OPENSSL_STRING_num(sigopts); i++) {
        char *sigopt = sk_OPENSSL_STRING_value(sigopts, i);
        if (pkey_ctrl_string(pkctx, sigopt) <= 0) {
            BIO_printf(err, "parameter error \"%s\"\n", sigopt);
            ERR_print_errors(bio_err);
            return 0;
        }
    }
    return 1;
}

int do_X509_CRL_sign(BIO *err, X509_CRL *x, EVP_PKEY *pkey, const EVP_MD *md,
                     STACK_OF(OPENSSL_STRING) *sigopts)
{
    int rv;
    EVP_MD_CTX mctx;
    EVP_MD_CTX_init(&mctx);
    rv = do_sign_init(err, &mctx, pkey, md, sigopts);
    if (rv > 0)
        rv = X509_CRL_sign_ctx(x, &mctx);
    EVP_MD_CTX_cleanup(&mctx);
    return rv > 0 ? 1 : 0;
}
