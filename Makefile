INCLUDE = -I.
CFLAGS = -O0 -g -Wall
LDFLAGS = -lssl -lcrypto

objects := $(patsubst %.c,%.o,$(wildcard *.c))

all: gencrl

gencrl: $(objects)
	$(CC) $(CFLAGS) $(INCLUDE) -o $@ $(objects) $(LDFLAGS)

clean:
	@rm -f *.o gencrl
